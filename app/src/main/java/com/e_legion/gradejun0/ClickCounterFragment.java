package com.e_legion.gradejun0;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ClickCounterFragment extends Fragment {

    @BindView(R.id.count)
    TextView mCountView;

    private int mCountClick;

    public static ClickCounterFragment newInstance() {
        ClickCounterFragment fragment = new ClickCounterFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fr_click_counter, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi();
    }

    @OnClick(R.id.add_count)
    void addCountTap() {
        mCountClick++;
        initUi();
    }

    private void initUi() {
        mCountView.setText(getString(R.string.count_click, mCountClick));
    }
}
