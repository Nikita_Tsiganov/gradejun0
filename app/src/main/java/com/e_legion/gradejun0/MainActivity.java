package com.e_legion.gradejun0;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_main);
        ButterKnife.bind(this);
        if (savedInstanceState == null)
            startFragment();
    }

    private void startFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fl_container, ClickCounterFragment.newInstance())
                .commit();
    }
}
